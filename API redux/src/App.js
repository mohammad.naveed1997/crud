
import './App.css';
import {useSelector,useDispatch} from 'react-redux'
import axios from 'axios';
function App() {
  
const state = useSelector(state => state)
 //console.log(state,'global state')
  const dispatch = useDispatch()
  
const login = async ()=> {
    
    const response = await axios.get('https://jsonplaceholder.typicode.com/todos/4');
    if(response.status === 200) {
      dispatch({type:"GET_DATA",payload:response.data})
    }
  }
  const post = async ()=>{
    const response =await axios.post('http://localhost:8000/register',state);
    console.log(response.data)
    // console.log(state)
    if(response.status === 200) {
      dispatch({type:'POST_DATA',payload:response.data})
    }
  }

  const Delete = async (email)=>{
    const response =await axios.delete('http://localhost:8000/delete',email);
    console.log(response.data)
    console.log(email)
    if(response.status === 200) {
      dispatch({type:'DELETE_DATA',payload:response.data})
    }
  }

  const update = async ()=>{
    const response =await axios.post('http://localhost:8000/update',state);
    console.log(response.data)
    //  console.log(state)
    if(response.status === 200) {
      dispatch({type:'POST_DATA',payload:response.data})
    }
  }

  return (
    <div className="App">
      <h2>{state.title ? state.title : 'no title'}</h2>
      <h2>{state.userId ? state.userId : 'no title'}</h2>
      <h2>{state.id ? state.id : 'no title'}</h2>
 
   <button onClick={login}>GET</button>

   <button onClick={post}>POST</button>

  <button onClick={update}>UPDATE</button> 

   <button onClick={Delete}>DELETE</button>

    </div>
  );
}

export default App;
