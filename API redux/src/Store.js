import {createStore} from 'redux'

const initalState = {
    username:"shake",
    password:"shake",
    email:"praneeth@123gmail.com",
    contact:"8962868282",
}

const reducer = (state = initalState,action) =>{
    if(action.type === 'GET_DATA') {
        return {
            ...state,
            ...action.payload
        }
    } else if(action.type ==='POST_DATA') {
        return {
            ...state,
            ...action.payload
        }
    } else if(action.type ==='UPDATE_DATA') {
        return {
            ...state,
            ...action.payload
        }
    }else if(action.type ==='DELETE_DATA') {
        return {
            ...state,
            ...action.payload
        }
    }
    return state;
}
export const store = createStore(reducer)

