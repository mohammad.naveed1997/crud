

import './App.css';
import Navabar from './components/Navabar';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './Pages/Home';
import Aboutus from './Pages/Aboutus';
import Contact from './Pages/Contact';
import Queries from './Pages/Queries';
import Login from './Pages/Login';
function App() {
  return (
    <div className="App">
   <Router>
   <Navabar/>
   <Switch>
     <Route path="/" exact component={Home}/>
     <Route path="/Aboutus" component={Aboutus}/>
     <Route patch="/Contact" component={Contact}/>
     <Route patch="/Queries" component={Queries}/>
     <Route patch="/Login" component={Login}/>
   </Switch>
   </Router>
    </div>
  );
}

export default App;
