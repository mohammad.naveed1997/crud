import React from 'react'
import { FaBars } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";
import {AiFillHome}from "react-icons/ai";
import {FcAbout}from "react-icons/fc";
import {IoIosContact}from "react-icons/io";
import {DiJqueryLogo}from "react-icons/di";
import {RiLoginCircleFill}from "react-icons/ri";

export const SiderbarData =[
    {
        title:'Home',
        path:'/',
        icons:<AiFillHome/>,
        cName:'nav-text'
    },
    {
        title:'Aboutus',
        path:'/Aboutus',
        icons:<FcAbout/>,
        cName:'nav-text'
    },

    {
        title:'Contact',
        path:'/Conatct',
        icons:<IoIosContact/>,
        cName:'nav-text'
    },

    {
        title:'Queries',
        path:'/Queries',
        icons:<DiJqueryLogo/>,
        cName:'nav-text'
    },

    {
        title:'Login',
        path:'/Login',
        icons:<RiLoginCircleFill/>,
        cName:'nav-text'
    },

]