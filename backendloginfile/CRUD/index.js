const bodyParser = require('body-parser');
const express = require('express');
const routes = require('./routes/index')
const mongoose=require('mongoose');
const db=require('./config/index').mongoURI;


const app = express();
mongoose.connect(db,{useNewUrlParser:true,useUnifiedTopology:true})
.then(()=> console.log('connected to db'))
.catch(err=> console.log(err.toString()))

app.use(bodyParser.json());


const PORT = process.env.PORT || 8000;

app.get('/', routes.index);
app.post('/register',routes.register)
app.get('/users', routes.users)
app.post('/delete',routes.delete)
app.post('/user',routes.user);
app.post('/update',routes.update) 
app.listen(PORT, ()=> console.log(`Listening at port ${PORT}`))