
import './App.css';
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Navbar1 from './components/pages/Layout/Navbar1';
import Notfound from './components/pages/Notfound';
import { BrowserRouter as Router, Route, Link,Switch} from "react-router-dom";
import Login from './components/Users/Login';
import Newaccount from './components/Users/Newaccount';
import { Container,Row,Col } from 'react-bootstrap';
import {FaQuoteRight,FaPhoneAlt} from "react-icons/fa"
import {GoLightBulb,GoMail} from "react-icons/go"
import {BiWorld} from "react-icons/bi"

function App() {


  return (
    
    <div className="page-container">
      <div className="content-wrap">
      <Router>
      <Navbar1/>
      <Switch>
      <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/login" component={Login}/>
        <Route exact path="/Newaccount" component={Newaccount}/> 
        <Route component={Notfound}/>
      
        </Switch>
        
    </Router>
    </div>
    </div>
    
  );
  
}

export default App;
