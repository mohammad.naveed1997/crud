import React from 'react'
import{Card,Button,Carousel,Row,Col,Container}from 'react-bootstrap'
export const About = () => {
    return (
        <div>
            <Container>
            <Row>
                <Col lg={4}>
<Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="https://st2.depositphotos.com/1141099/6198/i/600/depositphotos_61983329-stock-photo-historic-charminar.jpg" />
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Button variant="primary">Go somewhere</Button>
  </Card.Body>
</Card>
</Col>
<Col lg={8}>       
<h1>About page</h1>
            <p>The Story of My Experiments with Truth (Gujarati: Satya Na Prayogo athva Atmakatha, lit. 'Experiments with Truth or Autobiography') is the autobiography of Mohandas K. Gandhi, covering his life from early childhood through to 1921. It was written in weekly installments and published in his journal Navjivan from 1925 to 1929. Its English translation also appeared in installments in his other journal Young India.[1] It was initiated at the insistence of Swami Anand and other close co-workers of Gandhi, who encouraged him to explain the background of his public campaigns. </p>
<p>The Story of My Experiments with Truth (Gujarati: Satya Na Prayogo athva Atmakatha, lit. 'Experiments with Truth or Autobiography') is the autobiography of Mohandas K. Gandhi, covering his life from early childhood through to 1921. It was written in weekly installments and published in his journal Navjivan from 1925 to 1929. Its English translation also appeared in installments in his other journal Young India.[1] It was initiated at the insistence of Swami Anand and other close co-workers of Gandhi, who encouraged him to explain the background of his public campaigns. </p>
</Col>
</Row>
</Container>
        </div>
        
    )
}
export default About