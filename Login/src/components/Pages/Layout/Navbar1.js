import React from 'react'
import { NavDropdown, Navbar,Container,Button,Nav} from 'react-bootstrap';
import{TwitterOutlined,GoogleOutlined,LinkedinOutlined  } from '@ant-design/icons'
import {  Link,NavLink} from "react-router-dom";
export const Navbar1 = () => {
    return (
        <div>
            <Navbar bg="primary" variant="dark" expand="lg">
  <Container>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="me-auto">
        <NavLink className="nav-link" exact to="/">Home</NavLink>
        <NavLink className="nav-link" exact to="about">About</NavLink>
        <NavLink className="nav-link" exact to="contact">Contact</NavLink>
        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
          <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
        </NavDropdown>
      </Nav>
    </Navbar.Collapse>
  </Container>
  <div className="batman">
  <span  className="batman" ><TwitterOutlined /></span>
         <span  className="batman">|</span>
         <span  className="batman"><GoogleOutlined /></span>
         <span  className="batman">|</span>
         <span> <LinkedinOutlined /></span>
         </div>
  
  
 
  
  <Link className="btn btn-outline-light" to="login"> Login</Link>
</Navbar>



        </div>
    )
}
export default Navbar1