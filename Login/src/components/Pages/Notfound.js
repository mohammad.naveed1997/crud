import React from 'react'

export const Notfound = () => {
    return (
        <div className="notfound">
            <h1>page not Found</h1>
        </div>
    )
}
export default Notfound