import React from 'react'
import { Container,Row,Col,Form,FormControl,Button } from 'react-bootstrap';
import {FaQuoteRight,FaPhoneAlt} from "react-icons/fa"
import {GoLightBulb,GoMail} from "react-icons/go"
import {BiWorld} from "react-icons/bi"
import {RiTwitterFill,RiTwitterLine,RiWhatsappFill,RiInstagramLine,RiPencilLinet} from "react-icons/ri"
import { SocialIcon } from 'react-social-icons';
export const Footer = () => {
    return (
        <div className="footer">
              
      <Row >
        <Col lg={3}>
        <h5>203, Envato Labs, Behind Alis Steet</h5>
<br/>
<h5>Melbourne, Australia.</h5>
<h6><FaPhoneAlt/>123-456-789</h6>
<h6><GoMail/>contact@yourdomain.com</h6>
<h6><BiWorld/>www.yourdomain.com</h6>
        </Col>

        <Col lg={3}>
  <h3>Useful Link</h3><br/>
  <h5 className="spiderman">About us</h5>
  <h5 className="spiderman">Our courses</h5>
  <h5 className="spiderman">Pricing Table</h5>
  <h5 className="spiderman">Gallery</h5>
  <h5 className="spiderman">Shop</h5>
</Col>
<Col lg={3}>
  <h3>Twitter Feed</h3>
  <RiTwitterFill/>
<h6>Are you keen to learn some new</h6>
<h6>creative skills in 2021? We've done a</h6>
<h6> round up of the best free</h6>
<h6>@Photoshop courses a…</h6>
<h6 className="spiderman">Feb. 2, 2021</h6>
<br></br>
<br ></br>
<RiTwitterFill/>
<h6 >Are you Team Minimalism or Team</h6>
<h6>Maximalism? We put these</h6>
<h6> contrasting aesthetics to the test and</h6>
<h6>bring you some of t…</h6>
<h6 className="spiderman">Jan. 28, 2021</h6>
</Col>

<Col lg={3}>
<h3>Opening Hours</h3>
<h5 className="spiderman">About us</h5>
  <h5 className="spiderman">Mon-Tues : 6.00 am-10.00pm</h5>
  <h5 className="spiderman">Wednes-Thurs : 8.00am-6.00pm</h5>
  <h5 className="spiderman">Fri : 3.00pm-8.00pm</h5>
  <h5 className="spiderman">Sun : Closed</h5>
</Col>

      </Row>

<Row>
<Col  className="thanos" lg={4}>
<h3>Call Us Now</h3>
<h6>+91-7856584565</h6>
<h6>040-8565247</h6>
</Col>

<Col  className="thanos" lg={4}>
  <h1>Connect With Us</h1>
  <SocialIcon url="https://twitter.com/jaketrent" />
  <SocialIcon url="https://facebook.com/jaketrent" />
  <SocialIcon url="https://instagram.com/jaketrent" />
  <SocialIcon url="https://whatsapp.com/jaketrent" />
  <SocialIcon url="https://youtube.com/jaketrent" />

</Col>
<Col  className="thanos" lg={4}>
<h3>Subscribe Us</h3>
<Form className="d-flex">
      <FormControl
        type="search"
        placeholder="Search"
        className="mr-4"
        aria-label="Search"
      />
      <Button variant="outline-success">Search</Button>
    </Form>
</Col>

</Row>
      
  
        </div>
    )
}
export default Footer