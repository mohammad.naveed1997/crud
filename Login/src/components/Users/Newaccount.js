import React, { useState } from "react";
import axios from 'axios'
import { Link, useHistory } from "react-router-dom";
import { Avatar } from 'antd';
import { AntDesignOutlined } from '@ant-design/icons';
import { UserOutlined } from '@ant-design/icons';

const Newaccount= () => {
  let history = useHistory();
  const [user, setUser] = useState({
    
   username:"",
    email: "",  
    password:"",
    Conformpassword:""
  });

  const {  username, email,password, Conformpassword } = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const onSubmit = async e => {
    e.preventDefault();
    const response = await axios.post('http://localhost:8000/register',user);
    console.log(response,"response>>")
    if(response.status === 200) {
      history.push('/');
    } else {
      alert('something wrong')
    }
  };
  return (
    <div className="container">
      <div className="w-50 mx-auto shadow p-5">
        
        <form onSubmit={e => onSubmit(e)}>
        <div className="form-group">
        <h2 className="text-center mb-4">NewUsers</h2>
           <input
              type="username"
              className="form-control form-control-lg"
              placeholder="username"
              name="username"
              value={username}
              onChange={e => onInputChange(e)}
            />
          </div><br></br>

          <div className="form-group">
            <input
              type="email"
              className="form-control form-control-lg"
              placeholder="Enter Your E-mail Address"
              name="email"
              value={email}
              onChange={e => onInputChange(e)}
            /><br></br>
          </div>

          <div className="form-group">
            <input
              type="password"
              className="form-control form-control-lg"
              placeholder="Create Password"
              name="password"
              value={password}
              onChange={e => onInputChange(e)}
            />
          </div><br></br>
          
          <div className="form-group">
            <input
              type="password"
              className="form-control form-control-lg"
              placeholder="Conform Password"
              name="Conformpassword"
              value={ Conformpassword}
              onChange={e => onInputChange(e)}
            />
          </div><br></br>

         
         <button  className="btn btn-primary btn-block" onClick={onSubmit} >Create</button>
         
        </form>
      </div>
    </div>
  );
};

export default Newaccount;