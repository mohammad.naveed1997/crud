import React, { useState } from "react";
import axios from 'axios'
import { Link, useHistory } from "react-router-dom";
import { Avatar } from 'antd';
import { AntDesignOutlined } from '@ant-design/icons';
import { UserOutlined } from '@ant-design/icons';

const Login = () => {
  let history = useHistory();
  const [user, setUser] = useState({
    
    username: "",  
    password:""
  });


  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

 
  const onSubmit = async e => {
    e.preventDefault();
    const response = await axios.post('http://localhost:8000/register',user);
    console.log(response,"response>>")
    if(response.status === 200) {
      history.push('/');
    } else {
      alert('something wrong')
    }
  };
  return (
    <div className="container">
      <div className="w-50 mx-auto shadow p-5">
        
        <form onSubmit={e => onSubmit(e)}>
        <div className="form-group">
        <h2 className="text-center mb-4">Login</h2>
     
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Your Username"
              name="username"
              value={user.username}
              onChange={e => onInputChange(e)}
            />
          </div><br></br>

        

          <div className="form-group">
            <input
              type="password"
              className="form-control form-control-lg"
              placeholder="Enter Your password"
              name="password"
              value={user.password}
              onChange={e => onInputChange(e)}
            />
          </div><br></br>
          
         
          <button  className="btn btn-primary btn-block" onClick={onSubmit} >Login</button>
          <Link className='List' to="/Newaccount">Create New Account</Link>
        </form>
      </div>
    </div>
  );
};

export default Login;