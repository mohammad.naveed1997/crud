
import React from "react";
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./components/pages/Home";
import About from "./components/pages/About";
import Contact from "./components/pages/Contact";
import Navbar1 from "./components/Layout/Navbar1";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Notfound from "./components/pages/Notfound";
import Adduser from "./components/Users/Addusers";
import Edituser from "./components/Users/Edituser";
import User from "./components/Users/User";

function App(props) {
  return (
    <Router>
      <div className="App">
        <Navbar1 />

        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/users/add" component={Adduser} />
          <Route exact path="/users/edit/:id" component={Edituser} />
          <Route exact path="/users/:id" component={User} />
          <Route component={Notfound} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;